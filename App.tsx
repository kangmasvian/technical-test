import React from 'react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/screens/contacts/HomeScreen';
import AddContactScreen from './src/screens/contacts/AddContactScreen';
import EditContactScreen from './src/screens/contacts/EditContactScreen';
import store from './src/redux/store';

const Stack = createStackNavigator();

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }}/>
          <Stack.Screen name="AddContact" component={AddContactScreen} options={{ headerShown: false }}/>
          <Stack.Screen name="EditContact" component={EditContactScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;