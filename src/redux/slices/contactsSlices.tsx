import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { AppDispatch } from "../store";

interface Contact {
    id: string;
    firstName: string;
    lastName: string;
    age: number;
    photo: string;
}

interface contactsState {
    contacts: Contact[];
}

const initialState: contactsState = {
    contacts: []
}

const contactsSlice = createSlice({
    name: 'contacts',
    initialState,
    reducers: {
        setContacts(state, action: PayloadAction<Contact[]>) {
            state.contacts = action.payload;
        },
        addContact(state, action: PayloadAction<Contact>) {
            state.contacts.push(action.payload);
        },
        deleteContact(state, action: PayloadAction<string>) {
            state.contacts = state.contacts.filter(contact => contact.id !== action.payload);
        },
        updateContact(state, action: PayloadAction<Contact>) {
            state.contacts = state.contacts.map(contact => contact.id === action.payload.id ? action.payload : contact);
        }
    }
});

export const { setContacts, addContact, deleteContact, updateContact } = contactsSlice.actions;

export const fetchContacts = () => async (dispatch: AppDispatch) => {
    try {
      const response = await axios.get('https://contact.herokuapp.com/contact');
      dispatch(setContacts(response.data.data));
    } catch (error) {
      console.error('Failed to fetch contacts:', error);
    }
  };

export default contactsSlice.reducer;
export type { Contact, contactsState };