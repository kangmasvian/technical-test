import React, { useState } from 'react';
import { View, TextInput, Text, Alert, ImageBackground } from 'react-native';
import { Button as RNEButton } from '@rneui/themed'; // Updated import
import { useDispatch } from 'react-redux';
import { addContact } from '../../redux/slices/contactsSlices';
import { AppDispatch } from "../../redux/store";
import styles from './styles';

const AddContactScreen: React.FC<{ navigation: any }> = ({ navigation }) => {
  const dispatch = useDispatch<AppDispatch>();

  const [id, setId] = useState(''); // Updated
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [age, setAge] = useState('');
  const [photo, setPhoto] = useState('');

  const handleAddContact = () => {
    if (firstName && lastName && age) {
      const newContact = {
        id,
        firstName,
        lastName,
        age: parseInt(age, 10),
        photo: photo || 'N/A',
      };

      dispatch(addContact(newContact));
      navigation.goBack();
    } else {
      Alert.alert('Error', 'Please fill in all fields');
    }
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={{ uri: 'https://img.freepik.com/free-photo/abstract-luxury-soft-red-background-christmas-valentines-layout-design-studio-room-web-template-business-report-with-smooth-circle-gradient-color_1258-63942.jpg' }}
        style={styles.backgroundImage}
      >
        <View style={styles.titleContainer}>
        <Text style={styles.title}>Add Contact</Text>
        </View>
        <View style={styles.formContainer}>
          
        <TextInput
        style={styles.input}
        placeholder="First Name"
        value={firstName}
        onChangeText={setFirstName}
        placeholderTextColor={'black'}
      />
      <TextInput
        style={styles.input}
        placeholder="Last Name"
        value={lastName}
        onChangeText={setLastName}
        placeholderTextColor={'black'}
      />
      <TextInput
        style={styles.input}
        placeholder="Age"
        value={age}
        keyboardType="numeric"
        onChangeText={setAge}
        placeholderTextColor={'black'}
      />
      <TextInput
        style={styles.input}
        placeholder="Photo URL"
        value={photo}
        onChangeText={setPhoto}
        placeholderTextColor={'black'}
      />
      <RNEButton
        buttonStyle={styles.buttonWrapperStyle}
        title="Save"
        onPress={handleAddContact}
      />
        </View></ImageBackground>

    </View>
  );
};

export default AddContactScreen;