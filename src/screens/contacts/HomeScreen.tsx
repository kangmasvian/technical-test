import React, {useEffect} from "react";
import { View, Text, FlatList, Alert, ActivityIndicator } from 'react-native';
import { ListItem, Button as RNEButton, Avatar } from '@rneui/themed'; // Updated import
import { useDispatch, useSelector } from "react-redux";
import { RootState, AppDispatch } from "../../redux/store";
import { fetchContacts, deleteContact } from "../../redux/slices/contactsSlices";
import styles from './styles'; // Updated import

const HomeScreen: React.FC<{ navigation: any }> = ({ navigation }) => {
    const dispatch = useDispatch<AppDispatch>();
    const contacts = useSelector((state: RootState) => state.contacts.contacts);
    const [loading, setLoading] = React.useState(true);

    useEffect(() => {
        const loadContacts = async () => {
            await dispatch(fetchContacts());
            setLoading(false);
        }

        loadContacts();
    }, [dispatch]);

    const handleDelete = (id: string) => {
        dispatch(deleteContact(id));
    };

    const confirmDelete = (id: string) => {
      Alert.alert(
        'Confirm Delete',
        'Are you sure you want to delete this contact?',
        [
          {
            text: 'Cancel',
            style: 'cancel',
          },
          {
            text: 'Delete',
            onPress: () => handleDelete(id),
            style: 'destructive',
          },
        ],
        { cancelable: true }
      );
    };

      if (loading) {
        return <ActivityIndicator size="large" color="#0000ff" />;
      }

      return (
        <View style={styles.containerHome}>
          <View  style={{justifyContent:'center',alignItems:'center'}}>
          <Text style={{ fontSize: 24, fontWeight: 'bold', marginBottom: 10}}>
        Contact List
      </Text>
      </View>
      <FlatList
        data={contacts}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
            <ListItem bottomDivider onPress={() => navigation.navigate('EditContact', { contactId: item.id })}>
            <Avatar
              source={{ uri: item.photo != 'N/A' ? item.photo : 'https://via.placeholder.com/150' }}
              rounded
            />
            <ListItem.Content>
              <ListItem.Title>{`${item.firstName} ${item.lastName}`}</ListItem.Title>
              <ListItem.Subtitle>{item.age.toString()}</ListItem.Subtitle>
            </ListItem.Content>
            <RNEButton
              title="X"
              onPress={() => confirmDelete(item.id)}
              buttonStyle={styles.buttonDeleteStyle}
            />
          </ListItem>
        )}
      />
      <RNEButton
        buttonStyle={styles.buttonWrapperStyle}
        title="Add Contact"
        onPress={() => navigation.navigate('AddContact')}
      />
    </View>
      );
}

    export default HomeScreen;