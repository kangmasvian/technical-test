import React, { useState, useEffect } from 'react';
import { View, TextInput, Text, Alert,ImageBackground } from 'react-native';
import { Button as RNEButton } from '@rneui/themed'; // Updated import
import { useSelector, useDispatch } from 'react-redux';
import { RootState, AppDispatch } from "../../redux/store";
import { updateContact, Contact } from "../../redux/slices/contactsSlices";
import styles from './styles';

const EditContactScreen: React.FC<{ route: any, navigation: any }> = ({ route, navigation }) => {
  const { contactId } = route.params;
  const dispatch = useDispatch<AppDispatch>();

  const contact = useSelector((state: RootState) =>
    state.contacts.contacts.find(contact => contact.id === contactId)
  );

  const [firstName, setFirstName] = useState(contact?.firstName || '');
  const [lastName, setLastName] = useState(contact?.lastName || '');
  const [age, setAge] = useState(contact?.age.toString() || '');
  const [photo, setPhoto] = useState(contact?.photo || '');

  useEffect(() => {
    if (!contact) {
      Alert.alert('Error', 'Contact not found');
      navigation.goBack();
    }
  }, [contact, navigation]);

  const handleUpdateContact = () => {
    if (firstName && lastName && age) {
      const updatedContact: Contact = {
        id: contactId,
        firstName,
        lastName,
        age: parseInt(age, 10),
        photo: photo || 'N/A',
      };

      dispatch(updateContact(updatedContact));
      navigation.goBack();
    } else {
      Alert.alert('Error', 'Please fill in all fields');
    }
  };

  return (
    <View style={styles.container}>
            <ImageBackground
        source={{ uri: 'https://img.freepik.com/free-photo/abstract-luxury-soft-red-background-christmas-valentines-layout-design-studio-room-web-template-business-report-with-smooth-circle-gradient-color_1258-63942.jpg' }}
        style={styles.backgroundImage}
      >
        <View style={styles.titleContainer}>
        <Text style={styles.title}>Edit Contact</Text>
        </View>
        <View style={styles.formContainer}>
      <TextInput
        style={styles.input}
        placeholder="First Name"
        value={firstName}
        onChangeText={setFirstName}
        placeholderTextColor={'black'}
      />
      <TextInput
        style={styles.input}
        placeholder="Last Name"
        value={lastName}
        onChangeText={setLastName}
        placeholderTextColor={'black'}
      />
      <TextInput
        style={styles.input}
        placeholder="Age"
        value={age}
        keyboardType="numeric"
        onChangeText={setAge}
        placeholderTextColor={'black'}
      />
      <TextInput
        style={styles.input}
        placeholder="Photo URL"
        value={photo}
        onChangeText={setPhoto}
        placeholderTextColor={'black'}
      />

      <RNEButton
        buttonStyle={styles.buttonWrapperStyle}
        title="Update"
        onPress={handleUpdateContact}
      />
      </View></ImageBackground>
    </View>
  );
};

export default EditContactScreen;