import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');
const styles = StyleSheet.create({
    containerHome: {
        flex: 1,
        padding: 10,
        backgroundColor: '#fff',
      },
    container: {
        flex: 1,
      },
      backgroundImage: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
      },
      titleContainer: {
        flexDirection: 'row',  // Align items in a row
        alignItems: 'center',  // Center items vertically
        marginHorizontal: 20,
        marginTop: 50, // Adjust marginTop to move the title down
      },
      title: {
        fontSize: 24,
        fontWeight: 'bold',
        color: 'white',
        marginLeft: 10, // Add margin left to separate image and text
      },
      iconContainer: {
        alignItems: 'flex-end', // Align items to the right
        marginRight: 20, // Adjust marginRight for spacing
      },
      icon: {
        width: 200, // Adjust width and height as needed
        height: 200,
        resizeMode: 'cover', // Maintain aspect ratio
        marginBottom: 20, // Add margin bottom to separate from text
      },
      formContainer: {
        backgroundColor: 'white',
        padding: 20,
        margin: 20,
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
      },
      input: {
        height: 40,
        color: 'black',
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 10,
        paddingHorizontal: 10,
        borderRadius: 5,
      },
      button: {
        backgroundColor: '#007BFF',
        padding: 10,
        borderRadius: 5,
        alignItems: 'center',
      },
      buttonText: {
        color: 'white',
        fontWeight: 'bold',
      },
    buttonWrapperStyle: { height: 48, backgroundColor:'#cc1d06', borderRadius: 10, marginBottom: 10},
    buttonDeleteStyle: { width: 40, height: 48, backgroundColor:'#cc1d06', borderRadius: 10, marginBottom: 10},
  });

export default styles;