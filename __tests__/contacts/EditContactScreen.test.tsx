import React from "react";
import { render, fireEvent } from "@testing-library/react-native";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import EditContactScreen from "../../src/screens/contacts/EditContactScreen";
import { Contact } from "../../src/redux/slices/contactsSlices";

// Mock navigation and other dependencies inside jest.mock
jest.mock('../src/navigation', () => ({
  useNavigation: () => ({
    navigate: jest.fn(),
    goBack: jest.fn(),
  }),
}));

const mockStore = configureStore([]);

const initialContacts: Contact[] = [
  {
    id: '1',
    firstName: 'Frodo',
    lastName: 'Baggin',
    age: 30,
    photo: 'https://example.com/photo.jpg',
  },
];

describe("EditContactScreen", () => {
    let store: any;
    const navigation = { navigate: jest.fn(), goBack: jest.fn() };
    beforeEach(() => {
        store = mockStore({
            contacts: {
                contacts: initialContacts
            }
        });
    });

    it('should render correctly', () => {
      const { getByPlaceholderText, getByText } = render(
        <Provider store={store}>
          <EditContactScreen route={{ params: { contactId: '1' } }} navigation={navigation} />
        </Provider>
      );
  
    
        expect(getByPlaceholderText('First Name')).toBeTruthy();
        expect(getByPlaceholderText('Last Name')).toBeTruthy();
        expect(getByPlaceholderText('Age')).toBeTruthy();
        expect(getByPlaceholderText('Photo URL')).toBeTruthy();
        expect(getByText('Update Contact')).toBeTruthy();
      });

      it('should update a contact when all fields are filled and button is pressed', () => {
        const { getByPlaceholderText, getByText } = render(
          <Provider store={store}>
            <EditContactScreen route={{ params: { contactId: '1' } }} navigation={navigation} />
          </Provider>
        );
    
        fireEvent.changeText(getByPlaceholderText('First Name'), 'Alejandro');
        fireEvent.changeText(getByPlaceholderText('Last Name'), 'Garnacho');
        fireEvent.changeText(getByPlaceholderText('Age'), '21');
        fireEvent.changeText(getByPlaceholderText('Photo URL'), 'https://example.com/photo2.jpg');
    
        fireEvent.press(getByText('Update Contact'));
    
        expect(store.getActions()).toEqual([
          {
            payload: {
              id: '1',
              firstName: 'Alejandro',
              lastName: 'Garnacho',
              age: 21,
              photo: 'https://example.com/photo2.jpg',
            },
            type: 'contacts/updateContact',
          },
        ]);

        expect(jest.mocked(navigation.goBack)).toHaveBeenCalledTimes(1);
      });

      it('should show an alert if fields are not filled', () => {
        const { getByText } = render(
          <Provider store={store}>
            <EditContactScreen route={{ params: { contactId: '2' } }} navigation={navigation} />
          </Provider>
        );
    
        fireEvent.press(getByText('Update Contact'));
    
        expect(store.getActions()).toEqual([]);
        expect(jest.mocked(navigation.goBack)).toHaveBeenCalledTimes(0);
      });

    });