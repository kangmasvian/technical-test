import React from "react";
import { render, fireEvent } from "@testing-library/react-native";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import AddContactScreen from "../../src/screens/contacts/AddContactScreen";
import { AppDispatch } from "../../src/redux/store";

const mockStore = configureStore([]);
const navigation = { navigate: jest.fn(), goBack: jest.fn() };

describe("AddContactScreen", () => {
    let store: any;
    let dispatch: AppDispatch;

    beforeEach(() => {
        store = mockStore({
            contacts: {
                contacts: []
            }
        });
        dispatch = store.dispatch;
    });

    it('should render correctly', () => {
        const { getByPlaceholderText, getByText } = render(
          <Provider store={store}>
            <AddContactScreen navigation={navigation} />
          </Provider>
        );
    
        expect(getByPlaceholderText('First Name')).toBeTruthy();
        expect(getByPlaceholderText('Last Name')).toBeTruthy();
        expect(getByPlaceholderText('Age')).toBeTruthy();
        expect(getByPlaceholderText('Photo URL')).toBeTruthy();
        expect(getByText('Add Contact')).toBeTruthy();
      });

      it('should add a new contact when all fields are filled and button is pressed', () => {
        const { getByPlaceholderText, getByText } = render(
          <Provider store={store}>
            <AddContactScreen navigation={navigation} />
          </Provider>
        );
    
        fireEvent.changeText(getByPlaceholderText('First Name'), 'Frodo');
        fireEvent.changeText(getByPlaceholderText('Last Name'), 'Baggin');
        fireEvent.changeText(getByPlaceholderText('Age'), '30');
        fireEvent.changeText(getByPlaceholderText('Photo URL'), 'https://example.com/photo.jpg');
    
        fireEvent.press(getByText('Add Contact'));
    
        expect(dispatch).toHaveBeenCalledTimes(1);
        expect(navigation.goBack).toHaveBeenCalledTimes(1);
      });

      it('should show an alert if fields are not filled', () => {
        const { getByText } = render(
          <Provider store={store}>
            <AddContactScreen navigation={navigation} />
          </Provider>
        );
    
        fireEvent.press(getByText('Add Contact'));
    
        expect(dispatch).toHaveBeenCalledTimes(0);
        expect(navigation.goBack).toHaveBeenCalledTimes(0);
      });
    });